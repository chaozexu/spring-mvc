package com.qcby;

import com.qcby.mvc.CzxMvc;

public class Main {
    static {
        //获取包的全路径
        String path = Main.class.getResource("").getPath();

        //获取包名
        String packageName = Main.class.getPackage().getName();

        //扫描上面路径，包下的所有文件
        CzxMvc.scanner(path,packageName);
    }

    public static void main(String[] args) {
        //扫描完去对应map中找对应的类和对应的方法执行
        CzxMvc.exec("","");
        CzxMvc.exec("test","index1");
        CzxMvc.exec("test","");
        System.out.println("Hello World!");
    }

}
