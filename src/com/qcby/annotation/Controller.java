package com.qcby.annotation;

import java.lang.annotation.*;

/**
 * controller声明
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @ interface Controller {
}
